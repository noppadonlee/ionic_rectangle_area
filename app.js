const calculateBtn = document.getElementById('calc-btn');
const resetBtn = document.getElementById('reset-btn');
const widthInput = document.getElementById('width-input');
const lengthInput = document.getElementById('length-input');

const resultArea = document.getElementById('result');

const resetInputs = () => {
  widthInput.value = '';
  lengthInput.value = '';
};

const calculateArea = () => {
  const enteredWidth = +widthInput.value;
  const enteredLength = +lengthInput.value;

  const area = enteredWidth * enteredLength;

  if (widthInput.value == '' || lengthInput.value == '') {
    alert('กรุณาใส่ข้อมูล กว้าง/ยาว');
    return;
  }

  if (isNaN(area)) {
    alert('กรุณาใส่ตัวเลข');
    return;
  }

  const resultElement = document.createElement('ion-card');
  resultElement.innerHTML = `
    <ion-card-header>
    <ion-card-title>พื้นที่</ion-card-title>
    
    </ion-card-header>
    <ion-card-content>
      <h1><strong>${area}</strong></h1>
    </ion-card-content>
  `;
  resultArea.innerHTML = '';
  resultArea.appendChild(resultElement);

  widthInput.value = '';
  lengthInput.value = '';
};

calculateBtn.addEventListener('click', calculateArea);
resetBtn.addEventListener('click', resetInputs);